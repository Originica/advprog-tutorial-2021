package id.ac.ui.cs.tutorial0.service;

public interface AdventurerRankService {
    public String rankClassification(int power);
}
