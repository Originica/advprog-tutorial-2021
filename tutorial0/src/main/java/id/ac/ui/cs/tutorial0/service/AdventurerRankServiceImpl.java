package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

@Service
public class AdventurerRankServiceImpl implements AdventurerRankService {

    @Override
    public String rankClassification(int power) {
        if (power > 0 && power <= 20000) {
            return "C class";
        } else if (power > 20000 && power <= 100000) {
            return "B class";
        } else {
            return "A class";
        }
    }
}
